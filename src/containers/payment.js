import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Header from '../components/Header';
import PaymentForm from '../components/payment-form';


const Payment = () => 
	<div>
		<Header />
		<div className="container">
			<PaymentForm />
		</div>
	</div>;


const mapStateToProps = (state) => ({
  loanRequest: state.loanRequest
});

Payment.defaultProps = {
	loanRequest: {}
};

Payment.propTypes = {
	loanRequest: PropTypes.object
};

export default connect(mapStateToProps, null)(Payment);