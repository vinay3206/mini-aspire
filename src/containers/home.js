import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Header from '../components/Header';
import LoanRequest from '../components/loan-request';


const Home = ({ loanRequest: { isSubmitting, isSubmitted }}) => 
	<div>
		<Header />
		<div className="container">
			<LoanRequest isSubmitting={isSubmitting} isSubmitted={isSubmitted}/>
		</div>
	</div>;


const mapStateToProps = (state) => ({
  loanRequest: state.loanRequest
});

Home.defaultProps = {
	loanRequest: {}
};

Home.propTypes = {
	loanRequest: PropTypes.object
};

export default connect(mapStateToProps, null)(Home);