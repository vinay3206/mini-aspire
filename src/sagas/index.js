import { takeLatest, takeEvery } from 'redux-saga/effects';

import { loanRequest, payment } from '../actions';

import { loanRequestHandler, paymentHandler } from './loan-request';



const handlers = [{
		type: loanRequest.REQUEST,
		handler: loanRequestHandler,
	}, {
		type: payment.REQUEST,
		handler: paymentHandler
	}
];

export default function* root() {
  for (let i = 0; i < handlers.length; i += 1) {
    if (handlers[i].takeEvery) {
      yield takeEvery(handlers[i].type, handlers[i].handler);
    } else {
      yield takeLatest(handlers[i].type, handlers[i].handler);
    }
  }
}