import { put } from 'redux-saga/effects';
import { loanRequest, payment } from '../actions';



export function* loanRequestHandler(action) {
  try
  {
  	const { email, amount, duration } = action.payload;
  	alert(`${email} ${amount} ${duration}`);
    yield put({ type: loanRequest.SUCCESS, payload: { email, amount, duration: (duration*4) }});
  } catch (error) {
    yield put({ type: loanRequest.FAILURE });
  }
}

export function* paymentHandler(action) {
	try {
		const { email, amount } = action.payload;
		yield put({ type: payment.SUCCESS, payload: { email, amount }});
	} catch (error) {
		yield put({ type: payment.FAILURE });
	}
}