import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import HomePage from './containers/home';
import PaymentPage from './containers/payment';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Redirect exact from="/" to="/home" />
      <Route exact path="/home" component={HomePage} />
      <Route exact path="/pay" component={PaymentPage} />
    </Switch>
  </BrowserRouter>
);

export default App;