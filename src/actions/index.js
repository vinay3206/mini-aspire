import { createFormAction } from 'redux-form-saga';
import { SUBMIT_LOAN_REQUEST_FORM, SUBMIT_PAYMENT_FORM } from './types';


export const loanRequest = createFormAction(SUBMIT_LOAN_REQUEST_FORM);
export const payment = createFormAction(SUBMIT_PAYMENT_FORM);