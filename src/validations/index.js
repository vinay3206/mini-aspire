export const required = (value) =>
  value ? undefined : 'Required';

export const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Email address is not correct'
    : undefined;

export const minLength = (min) => (value) =>
  value && value.length < min
    ? 'Minimum length'
    : undefined;


export const positiveNumber = (value) =>
  value
    ? parseInt(value, 10) > 0
      ? undefined
      : 'Must be a positve numer'
    : undefined;