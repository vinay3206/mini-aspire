import React from 'react';
import { Button, Form, FormGroup, Label } from 'reactstrap';
import { Field, reduxForm } from 'redux-form';

import { payment } from '../../actions';
import renderField from '../Input';
import { required, email, positiveNumber } from '../../validations';


export class PaymentForm extends React.Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <Form onSubmit={handleSubmit(payment)}>
        <FormGroup>
          <Label for="email">Email</Label>
          <Field name="email" id="email" component={renderField} type="email" placeholder="Your Email" className="form-control" validate={[ required, email ]}/>
        </FormGroup>
        <FormGroup>
          <Label for="amount">Amount to be paid</Label>
          <Field name="amount" id="amount" component={renderField} type="number" placeholder="amount to be paid" className="form-control" validate={[ required, positiveNumber ]}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'payment-form'
})(PaymentForm);
