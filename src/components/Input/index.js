import React from 'react';
import { Input } from 'reactstrap';

export default ({ input, placeholder, type, meta: { touched, error, warning }, children }) => (
  <span>
      <Input autoComplete="off" className={ type === 'select' ? 'input select' : 'input'} {...input} placeholder={placeholder} type={type} { ...(touched && (error && { valid: false }))}>{children}</Input>
  </span>
);