import React from 'react';
import { Button, Form, FormGroup, Label, Alert} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';

import { loanRequest } from '../../actions';
import renderField from '../Input';
import { required, email, positiveNumber } from '../../validations';


export class LoanRequest extends React.Component {
  render() {
    const { handleSubmit, isSubmitted, isSubmitting } = this.props;

    return (
      <Form onSubmit={handleSubmit(loanRequest)}>
        {isSubmitting && 
          <Alert color="warning">
            Processing
          </Alert>
        }
        {
          isSubmitted &&
          <Alert color="success">
            Success
          </Alert>
        }
        <FormGroup>
          <Label for="email">Email</Label>
          <Field name="email" id="email" component={renderField} type="email" placeholder="Your Email" className="form-control" validate={[ required, email ]}/>
        </FormGroup>
        <FormGroup>
          <Label for="amount">Loan Amount</Label>
          <Field name="amount" id="amount" component={renderField} type="number" placeholder="Loan amount" className="form-control" validate={[ required, positiveNumber ]}/>
        </FormGroup>
        <FormGroup>
          <Label for="duration">Loan Duration</Label>
          <Field name="duration" id="duration" component={renderField} type="number" placeholder="Ex: 12(in Months)" className="form-control" validate={[ required, positiveNumber ]}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'loan-request'
})(LoanRequest);