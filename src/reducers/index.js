import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import loanRequest from './loan-request';

const appReducer = combineReducers({
  form: formReducer,
  loanRequest,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
