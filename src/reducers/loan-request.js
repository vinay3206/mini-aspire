import { loanRequest, payment } from '../actions';

export const initialState = {
  requests: [],
  inSubmitting: false,
  isSubmitted: false,
};

export default function loanRequestReducer(state = initialState, action) {
    switch(action.type) {
        case loanRequest.SUCCESS: {
             const newObject = action.payload
            return { ...state, requests: [...state.requests, newObject], isSubmitted: true, isSubmiting: false};
        }
        case loanRequest.REQUEST: {
            return { ...state, isSubmitted: false, isSubmiting: true };
        }
        case payment.SUCCESS: {
            const { amount, email } = action.payload;
            const updatedRequests = state.requests.map((request) => {
                if(request.email === email) {
                    const updatedRequest = { ...request, amount: request.amount - amount, duration: request.duration -1 };
                    return updatedRequest;
                }
                return request;
            });
            return { ...state, requests: updatedRequests };
        }
        default:
            return state;
    }
}
